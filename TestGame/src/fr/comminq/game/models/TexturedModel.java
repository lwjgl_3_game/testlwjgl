package fr.comminq.game.models;

import fr.comminq.game.textures.ModelTexture;

public class TexturedModel {

	private TestModel model;
	private	ModelTexture texture;
	
	public TexturedModel(TestModel model, ModelTexture texture) {
		this.model = model;
		this.texture = texture;
	}
	
	public TestModel getModel() {
		return model;
	}
	
	public ModelTexture getTexture() {
		return texture;
	}
	
}
