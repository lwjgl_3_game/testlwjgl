package fr.comminq.game.skybox;

import org.joml.Matrix4f;

import fr.comminq.game.entities.Camera;
import fr.comminq.game.shaders.ShaderProgram;
import fr.comminq.game.utils.Maths;

public class SkyboxShader extends ShaderProgram{
 
    private static final String VERTEX_FILE = "src/fr/comminq/game/skybox/vertexShader.txt";
    private static final String FRAGMENT_FILE = "src/fr/comminq/game/skybox/fragmentShader.txt";
     
    private int location_projectionMatrix;
    private int location_viewMatrix;
     
    public SkyboxShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }
     
    public void loadProjectionMatrix(Matrix4f matrix){
        super.injectMatrix(location_projectionMatrix, matrix);
    }
 
    public void loadViewMatrix(Camera camera){
        Matrix4f matrix = Maths.createViewMatrix(camera);
        matrix._m30(0);
        matrix._m31(0);
        matrix._m32(0);
        super.injectMatrix(location_viewMatrix, matrix);
    }
     
    @Override
    protected void getAllUniformLocations() {
        location_projectionMatrix = super.getUniformLocation("projectionMatrix");
        location_viewMatrix = super.getUniformLocation("viewMatrix");
    }
 
    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position");
    }
 
}