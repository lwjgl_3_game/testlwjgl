package fr.comminq.game;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import fr.comminq.game.cubemap.TextureData;
import fr.comminq.game.models.TestModel;
import fr.comminq.game.utils.TexturesLoader;
import fr.comminq.game.utils.Triplet;

public class TestModelLoader {

	public static final int VERTEX_POSITION_LENGTH = 3;
	public static final int TEXTURE_POSITION_LENGTH = 2;

	private List<Integer> vaos;
	private List<Integer> vbos;
	private List<Integer> textures;

	public TestModelLoader() {
		this.vaos = new ArrayList<>();
		this.vbos = new ArrayList<>();
		this.textures = new ArrayList<>();
	}

	public void clearVaosAndVbos() {
		for (Integer integer : this.vaos) {
			GL30.glDeleteVertexArrays(integer);
		}
		for (Integer integer : this.vbos) {
			GL15.glDeleteBuffers(integer);
		}
		for (Integer integer : this.textures) {
			GL11.glDeleteTextures(integer);
		}
	}
	
	public int loadCubeMap(String[] textureFiles) {
		int texID = GL11.glGenTextures();
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL13.GL_TEXTURE_CUBE_MAP, texID);
		for(int i = 0; i < textureFiles.length; i++) {
			TextureData data = decodeTextureFile("skybox/"+textureFiles[i]);
			GL11.glTexImage2D(
					GL13.GL_TEXTURE_CUBE_MAP_POSITIVE_X+i,
					0,
					GL11.GL_RGBA8,
					get2Fold(data.getWidth()),
					get2Fold(data.getHeight()),
					0,
					GL11.GL_RGBA,
					GL11.GL_UNSIGNED_BYTE,
					data.getBuffer());
		}
		GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
		GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
		textures.add(texID);
		return texID;
	}
	
	private TextureData decodeTextureFile(String fileName) {
		int width = 0;
		int height = 0;
		ByteBuffer buffer = null;
		try {
			Triplet<ByteBuffer, Integer, Integer> triplet = TexturesLoader.loadImage("resources/"+fileName+".png");
			buffer = triplet.getT1();
			width = triplet.getT2();
			height = triplet.getT3();
		}catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return new TextureData(width, height, buffer);
	}

	public int loadTexture(final String path) {
		int texture = 0;
		try {
			
			Triplet<ByteBuffer, Integer, Integer> triplet = TexturesLoader.loadImage("resources/"+path+".png");
			ByteBuffer buffer = triplet.getT1();
			int width = triplet.getT2();
			int height = triplet.getT3();
			
			texture = GL11.glGenTextures();
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture);
			GL11.glTexImage2D(
					GL11.GL_TEXTURE_2D,
					0,
					GL11.GL_RGBA8,
					get2Fold(width),
					get2Fold(height),
					0,
					GL11.GL_RGBA,
					GL11.GL_UNSIGNED_BYTE,
					buffer);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
		return texture;
	}
	
	private int get2Fold(int fold) {
        int ret = 2;
        while (ret < fold) {
            ret *= 2;
        }
        return ret;
    }

	public TestModel loadToVAO(float[] positions, int dimension) {
		int vaoID = createVAO();
		storeDataInAttrList(0, dimension, positions);
		unbindVAO();
		return new TestModel(vaoID, positions.length / dimension);
	}
	
	public TestModel loadToVAO(float[] positions, float[] textureCoords, float[] normals, int[] indices) {
		int vaoID = createVAO();
		bindIndicesBuffer(indices);
		storeDataInAttrList(0, VERTEX_POSITION_LENGTH, positions);
		storeDataInAttrList(1, TEXTURE_POSITION_LENGTH, textureCoords);
		storeDataInAttrList(2, VERTEX_POSITION_LENGTH, normals);
		unbindVAO();
		return new TestModel(vaoID, indices.length);
	}

	private int createVAO() {
		int vaoID = GL30.glGenVertexArrays();
		this.vaos.add(vaoID);
		GL30.glBindVertexArray(vaoID);
		return vaoID;
	}

	private void storeDataInAttrList(int attNumber, int coordSize, float[] data) {
		int vboID = GL15.glGenBuffers();
		this.vbos.add(vboID);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
		FloatBuffer fb = toFloatBuffer(data);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, fb, GL15.GL_STATIC_DRAW);
		GL20.glVertexAttribPointer(attNumber, coordSize, GL20.GL_FLOAT, false, 0, 0);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
	}

	private void bindIndicesBuffer(int[] indices) {
		int vboID = GL15.glGenBuffers();
		this.vbos.add(vboID);
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboID);
		IntBuffer buffer = toIntBuffer(indices);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
	}

	private IntBuffer toIntBuffer(int[] data) {
		IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}

	private FloatBuffer toFloatBuffer(float[] data) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}

	private void unbindVAO() {
		GL30.glBindVertexArray(0);
	}

}
