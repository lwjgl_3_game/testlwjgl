package fr.comminq.game.textures;

public class ModelTexture {

	private int textureID;
	
	private float shineDamper;
	private float reflectivity;

	public ModelTexture(int textureID) {
		this.textureID = textureID;
		this.shineDamper = 1;
		this.reflectivity = 0;
	}

	public int getTextureID() {
		return textureID;
	}

	public float getShineDamper() {
		return shineDamper;
	}

	public float getReflectivity() {
		return reflectivity;
	}
	
	public void setShineDamper(float shineDamper) {
		this.shineDamper = shineDamper;
	}
	
	public void setReflectivity(float reflectivity) {
		this.reflectivity = reflectivity;
	}
	
	
	
	
	
}
