package fr.comminq.game.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import fr.comminq.game.cubemap.PNGDecoder;
import fr.comminq.game.cubemap.PNGDecoder.Format;

public class TexturesLoader {

	public static Triplet<ByteBuffer, Integer, Integer> loadImage(String path) throws IOException{
		FileInputStream fis = new FileInputStream(path);
		PNGDecoder decod = new PNGDecoder(fis);
		int width = decod.getWidth();
		int height = decod.getHeight();
		ByteBuffer buffer = ByteBuffer.allocateDirect(4 * width * height);
		decod.decode(buffer, width * 4, Format.RGBA);
		buffer.flip();
		fis.close();
		return new Triplet<ByteBuffer, Integer, Integer>(buffer, width, height);
	}
	
}
