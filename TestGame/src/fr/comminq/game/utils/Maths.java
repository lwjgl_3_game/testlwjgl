package fr.comminq.game.utils;

import org.joml.Matrix4f;
import org.joml.Vector3f;

import fr.comminq.game.entities.Camera;

public class Maths {

	public static Matrix4f createTransformMatrix(Vector3f translation, float rx, float ry, float rz, float scale) {
		return new Matrix4f().translate(translation)
				.rotateXYZ((float) Math.toRadians(rx), (float) Math.toRadians(ry), (float) Math.toRadians(rz))
				.scale(scale);
	}
	
	public static Vector3f calcNormal(Vector3f v0, Vector3f v1, Vector3f v2) {
		Vector3f tangentA = v0.sub(v1, new Vector3f());
		Vector3f tangentB = v0.sub(v2, new Vector3f());
		Vector3f normal = tangentA.cross(tangentB, new Vector3f());
		return normal.normalize();
	}

	public static Matrix4f createViewMatrix(Camera camera) {
		Vector3f cameraPos = camera.getPosition();
		Vector3f negativeCameraPos = new Vector3f(-cameraPos.x, -cameraPos.y, -cameraPos.z);
		return new Matrix4f()
				.rotateX((float) Math.toRadians(camera.getPitch()))
				.rotateY((float) Math.toRadians(camera.getYaw()))
				.translate(negativeCameraPos);
	}

}
