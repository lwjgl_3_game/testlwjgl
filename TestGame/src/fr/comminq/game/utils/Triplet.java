package fr.comminq.game.utils;

public class Triplet<S1, S2, S3> {

	private S1 t1;
	private S2 t2;
	private S3 t3;

	public Triplet(S1 t1, S2 t2, S3 t3) {
		super();
		this.t1 = t1;
		this.t2 = t2;
		this.t3 = t3;
	}

	public S1 getT1() {
		return t1;
	}

	public void setT1(S1 t1) {
		this.t1 = t1;
	}

	public S2 getT2() {
		return t2;
	}

	public void setT2(S2 t2) {
		this.t2 = t2;
	}

	public S3 getT3() {
		return t3;
	}

	public void setT3(S3 t3) {
		this.t3 = t3;
	}

	@Override
	public String toString() {
		return "Triplet [t1=" + t1 + ", t2=" + t2 + ", t3=" + t3 + "]";
	}
	
	

}
