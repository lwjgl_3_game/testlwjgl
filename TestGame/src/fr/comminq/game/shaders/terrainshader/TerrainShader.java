package fr.comminq.game.shaders.terrainshader;

import org.joml.Matrix4f;

import fr.comminq.game.entities.Camera;
import fr.comminq.game.light.Light;
import fr.comminq.game.shaders.ShaderProgram;
import fr.comminq.game.utils.Maths;

public class TerrainShader extends ShaderProgram{

	private static final String FRAGMENT_FILE = "src/fr/comminq/game/shaders/terrainshader/fragmentShader.txt";
	private static final String VERTEX_FILE = "src/fr/comminq/game/shaders/terrainshader/vertexShader.txt";

	private int loc_transformationMatrix;
	private int loc_projectionMatrix;
	private int loc_viewMatrix;
	private int loc_lightPosition;
	private int loc_lightColour;
	private int loc_shineDamper;
	private int loc_reflectivity;
	
	public TerrainShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
		super.bindAttribute(1, "textureCoords");
		super.bindAttribute(2, "normal");
	}

	@Override
	protected void getAllUniformLocations() {
		this.loc_transformationMatrix = super.getUniformLocation("transformationMatrix");
		this.loc_projectionMatrix = super.getUniformLocation("projectionMatrix");
		this.loc_viewMatrix = super.getUniformLocation("viewMatrix");
		this.loc_lightColour = super.getUniformLocation("lightColour");
		this.loc_lightPosition = super.getUniformLocation("lightPosition");

		this.loc_shineDamper = super.getUniformLocation("shineDamper");
		this.loc_reflectivity = super.getUniformLocation("reflectivity");
	}

	public void loadShineVars(float damper, float reflect) {
		super.injectFloat(loc_shineDamper, damper);
		super.injectFloat(loc_reflectivity, reflect);
	}
	
	public void loadLight(Light light) {
		super.injectVector(this.loc_lightPosition, light.getPosition());
		super.injectVector(this.loc_lightColour, light.getColour());
	}
	
	public void loadViewMatrix(Camera camera) {
		super.injectMatrix(this.loc_viewMatrix, Maths.createViewMatrix(camera));
	}
	
	public void loadTransformationMatrix(Matrix4f matrix) {
		super.injectMatrix(this.loc_transformationMatrix, matrix);
	}
	
	public void loadProjectionMatrix(Matrix4f matrix) {
		super.injectMatrix(this.loc_projectionMatrix, matrix);
	}
	
}
