package fr.comminq.game.shaders;

import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.FloatBuffer;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

public abstract class ShaderProgram {

	private static FloatBuffer MATRIX_BUFFER = BufferUtils.createFloatBuffer(16);
	
	private int programID;
	private int vertexShaderID;
	private int fragmentShaderID;
	
	public ShaderProgram(String vertexFile, String fragmentFile) {
		this.vertexShaderID = loadShader(vertexFile, GL20.GL_VERTEX_SHADER);
		this.fragmentShaderID = loadShader(fragmentFile, GL20.GL_FRAGMENT_SHADER);
		this.programID = GL20.glCreateProgram();
		GL20.glAttachShader(this.programID, this.vertexShaderID);
		GL20.glAttachShader(this.programID, this.fragmentShaderID);
		bindAttributes();
		GL20.glLinkProgram(this.programID); 
		GL20.glValidateProgram(this.programID);
		getAllUniformLocations();
	}
	
	protected abstract void getAllUniformLocations();
	
	protected void injectFloat(int location, float value) {
		GL20.glUniform1f(location, value);
	}
	
	protected void injectVector(int location, Vector3f value) {
		GL20.glUniform3f(location, value.x, value.y, value.z);
	}
	
	protected void injectBoolean(int location, boolean value) {
		float load = 0;
		if(value) load = 1;
		injectFloat(location, load);
	}
	
	protected void injectMatrix(int location, Matrix4f matrix) {
		GL20.glUniformMatrix4fv(location, false, matrix.get(MATRIX_BUFFER));
	}
	
	protected int getUniformLocation(String uniformName) {
		return GL20.glGetUniformLocation(this.programID, uniformName);
	}
	
	public void cleanUp() {
		stop();
		GL20.glDetachShader(this.programID, this.fragmentShaderID);
		GL20.glDetachShader(this.programID, this.vertexShaderID);
		GL20.glDeleteShader(this.fragmentShaderID);
		GL20.glDeleteShader(this.vertexShaderID);
		GL20.glDeleteProgram(this.programID);
	}
	
	public void start() {
		GL20.glUseProgram(this.programID);
	}
	
	public void stop() {
		GL20.glUseProgram(0);
	}
	
	protected void bindAttribute(int attribute, String variableName) {
		GL20.glBindAttribLocation(this.programID, attribute, variableName);
	}
	
	protected abstract void bindAttributes();
	
	public int getVertexShaderID() {
		return vertexShaderID;
	}
	
	public int getFragmentShaderID() {
		return fragmentShaderID;
	}
	
	private static int loadShader(String file, int type) {
		StringBuilder shaderSource = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line;
			while((line = reader.readLine()) != null) {
				shaderSource.append(line).append("\n");
			}
			reader.close();
		} catch (Exception e) {
			System.err.println("Fichier inconnu");
			e.printStackTrace();
			System.exit(-1);
		}
		int shaderID = GL20.glCreateShader(type);
		GL20.glShaderSource(shaderID, shaderSource);
		GL20.glCompileShader(shaderID);
		if(GL20.glGetShaderi(shaderID, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
			System.out.println(GL20.glGetShaderInfoLog(shaderID, 500));
			System.err.println("Erreur compilation shader");
			System.exit(-1);
		}
		return shaderID;
	}
	
}
