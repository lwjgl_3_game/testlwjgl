package fr.comminq.game;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.GLFW_CONTEXT_VERSION_MAJOR;
import static org.lwjgl.glfw.GLFW.GLFW_CONTEXT_VERSION_MINOR;
import static org.lwjgl.glfw.GLFW.GLFW_FALSE;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_CORE_PROFILE;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_PROFILE;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_TRUE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
import static org.lwjgl.glfw.GLFW.glfwGetWindowSize;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.joml.Vector3f;
import org.lwjgl.Version;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLCapabilities;
import org.lwjgl.system.MemoryStack;

import fr.comminq.game.entities.Camera;
import fr.comminq.game.entities.Entity;
import fr.comminq.game.entities.MainPlayer;
import fr.comminq.game.light.Light;
import fr.comminq.game.models.TestModel;
import fr.comminq.game.models.TexturedModel;
import fr.comminq.game.renderer.MasterRender;
import fr.comminq.game.textures.ModelTexture;
import fr.comminq.game.utils.InputHandler;
import fr.comminq.game.utils.OBJLoader; 

public class Game {

	// The window handle
	private long window;

	public static final int WIDTH = 1280;
	public static final int HEIGHT = 720;
	public static final int FOV = 90;
	public static final float NEAR_PLANE = 0.1f;
	public static final float FAR_PLANE = 1000;
	
	
	public void run() {
		System.out.println("Hello LWJGL " + Version.getVersion() + "!");

		init();
		loop();

		// Free the window callbacks and destroy the window
		glfwFreeCallbacks(window);
		glfwDestroyWindow(window);

		// Terminate GLFW and free the error callback
		glfwTerminate();
		glfwSetErrorCallback(null).free();
	}

	private void init() {
		GLFWErrorCallback.createPrint(System.err).set();

		if ( !glfwInit() )
			throw new IllegalStateException("Unable to initialize GLFW");

		// Configure GLFW
		glfwDefaultWindowHints(); // optional, the current window hints are already the default
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable
		
		// Compatibilité pour le Linux
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		// Create the window
		window = glfwCreateWindow(WIDTH, HEIGHT, "Hello World!", NULL, NULL);
		if ( window == NULL )
			throw new RuntimeException("Failed to create the GLFW window");

		
		InputHandler.init(window);

		// Get the thread stack and push a new frame
		try ( MemoryStack stack = stackPush() ) {
			IntBuffer pWidth = stack.mallocInt(1); // int*
			IntBuffer pHeight = stack.mallocInt(1); // int*

			// Get the window size passed to glfwCreateWindow
			glfwGetWindowSize(window, pWidth, pHeight);

			// Get the resolution of the primary monitor
			GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

			// Center the window
			glfwSetWindowPos(
				window,
				(vidmode.width() - pWidth.get(0)) / 2,
				(vidmode.height() - pHeight.get(0)) / 2
			);
		} // the stack frame is popped automatically

		// Make the OpenGL context current
		glfwMakeContextCurrent(window);
		// Enable v-sync
		glfwSwapInterval(1);
		// Make the window visible
		glfwShowWindow(window);
		// Init Input key & mouse handle
		
	}
	
	private void loop() {
		// This line is critical for LWJGL's interoperation with GLFW's
		GLCapabilities gl = GL.createCapabilities();

		// Set the clear color
		glClearColor(1.0f, 0.0f, 0.0f, 0.0f);

		TestModelLoader loader = new TestModelLoader();
		TestModel model = OBJLoader.loadObjModel("arbre", loader);
		//TestModel model = OBJLoader.loadObjModel("test_model", loader);
		ModelTexture texture = new ModelTexture(loader.loadTexture("arbre"));
		TexturedModel modelTexture = new TexturedModel(model, texture);
		
		//modelTexture.getTexture().setShineDamper(25);
		//modelTexture.getTexture().setReflectivity(4);
		
		
		
		List<Entity> toRender = new ArrayList<>();
		toRender.add(new Entity(modelTexture, new Vector3f(0, 0, 0), 0, 0, 0, 1));
		toRender.add(new Entity(modelTexture, new Vector3f(0, 25, -25), 0, 0, 0, 1));
		toRender.add(new Entity(modelTexture, new Vector3f(25, 10, 30), 0, 0, 0, 1));
		toRender.add(new Entity(modelTexture, new Vector3f(-30, -5, 35), 0, 0, 0, 1));
		
		
		Light light = new Light(new Vector3f(0, -50, 0), new Vector3f(1, 1, 1));
		
		MainPlayer mp = new MainPlayer(modelTexture, new Vector3f(0, 0, 0), 0f, 0f, 0f, 0.5f);
		toRender.add(mp);
		Camera camera = new Camera(mp);
		
		MasterRender renderer = new MasterRender(loader);
	
		// Ajout du wireframe (ne fait le rendu que des traits des triangles et non l'intérieur
		renderer.setWireFrame(false);
			
		while ( !glfwWindowShouldClose(window) ) {
			InputHandler.update();
			mp.move();
			camera.move();
			for(Entity entity : toRender) {
				renderer.processEntity(entity);
			}
			renderer.render(light, camera);
			
			glfwSwapBuffers(window);
		}
		renderer.cleanUp();
		loader.clearVaosAndVbos();
	}

	public static void main(String[] args) {
		new Game().run();
	}

}