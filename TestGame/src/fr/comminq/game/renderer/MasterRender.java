package fr.comminq.game.renderer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joml.Matrix4f;
import org.lwjgl.opengl.GL11;

import fr.comminq.game.Game;
import fr.comminq.game.TestModelLoader;
import fr.comminq.game.entities.Camera;
import fr.comminq.game.entities.Entity;
import fr.comminq.game.light.Light;
import fr.comminq.game.models.TexturedModel;
import fr.comminq.game.shaders.staticshader.StaticShader;
import fr.comminq.game.shaders.terrainshader.TerrainShader;
import fr.comminq.game.skybox.SkyBoxRender;
import fr.comminq.game.terrain.Terrain;

public class MasterRender {

	private StaticShader shader;
	private EntityRender render;
	private Map<TexturedModel, List<Entity>> map;
	private List<Terrain> terrains;
	private Matrix4f projectionMatrix;
	
	private TerrainRender terrainRender;
	private TerrainShader terrainShader;
	private SkyBoxRender skyBoxRender;
	private boolean wireFrame;

	public static final float RED = 0.3f;
	public static final float GREEN = 0.3f;
	public static final float BLUE = 0.34444f;
	
	public MasterRender(TestModelLoader loader) {
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glCullFace(GL11.GL_BACK);
		GL11.glEnable( GL11.GL_NORMALIZE );
		createProjectionMatrix();
		this.shader = new StaticShader();
		this.render = new EntityRender(shader, projectionMatrix);
		this.map = new HashMap<>();
		this.terrainShader = new TerrainShader();
		this.terrainRender = new TerrainRender(this.terrainShader, projectionMatrix);
		this.terrains = new ArrayList<>();
		this.skyBoxRender = new SkyBoxRender(loader, this.projectionMatrix);
	}
	
	public boolean isWireFrame() {
		return wireFrame;
	}
	
	public void setWireFrame(boolean wireFrame) {
		this.wireFrame = wireFrame;
	}
	
	public void render(Light light, Camera camera) {
		prepare();
		shader.start();
		shader.loadSkyColour(RED, GREEN, BLUE);
		shader.loadLight(light);
		shader.loadViewMatrix(camera);
		render.render(this.map);
		shader.stop();

		terrainShader.start();
		terrainShader.loadLight(light);
		terrainShader.loadViewMatrix(camera);
		terrainRender.render(terrains);
		terrainShader.stop();
		
		end();
		skyBoxRender.render(camera);
		
		terrains.clear();
		map.clear();
		
	}
	
	public void processTerrain(Terrain terrain) {
		this.terrains.add(terrain);
	}
	
	public void end() {
		if(this.wireFrame) {
			GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);
		}
        
	}
	
	public void prepare() {
		GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT|GL11.GL_DEPTH_BUFFER_BIT);
        GL11.glClearColor(RED, GREEN, BLUE, 1);
        
        if(this.wireFrame) {
        	GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
        }
        
	}
	
	public void processEntity(Entity entity) {
		TexturedModel key = entity.getModel();
		List<Entity> batch = this.map.get(key);
		if(batch == null) {
			batch = new ArrayList<Entity>();
			batch.add(entity);
			this.map.put(key, batch);
		}else {
			batch.add(entity);
		}
	}
	
	public void cleanUp() {
		shader.cleanUp();
		terrainShader.cleanUp();
	}
	
	public Map<TexturedModel, List<Entity>> getMap() {
		return map;
	}
	
	public EntityRender getRender() {
		return render;
	}
	
	public StaticShader getShader() {
		return shader;
	}
	
	private void createProjectionMatrix(){
        float aspectRatio = (float) Game.WIDTH / (float) Game.HEIGHT;
        float y_scale = (float) ((1f / Math.tan(Math.toRadians(Game.FOV / 2f))) * aspectRatio);
        float x_scale = y_scale / aspectRatio;
        float frustum_length = Game.FAR_PLANE - Game.NEAR_PLANE;
 
        projectionMatrix = new Matrix4f();
        projectionMatrix._m00(x_scale);
        projectionMatrix._m11( y_scale);
        projectionMatrix._m22(-((Game.FAR_PLANE + Game.NEAR_PLANE) / frustum_length));
        projectionMatrix._m23(-1);
        projectionMatrix._m32(-((2 * Game.NEAR_PLANE * Game.FAR_PLANE) / frustum_length));
        projectionMatrix._m33(0);
    }

	
}
