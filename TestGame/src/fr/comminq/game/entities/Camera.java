package fr.comminq.game.entities;

import org.joml.Vector3f;

import fr.comminq.game.utils.InputHandler;
import fr.comminq.game.utils.Triplet;

public class Camera {

	public static final float SPEED = 0.3f;
	public static final float MOUSE_SENS = 0.15f;
	
	public static final float DISTANCE_BETWEEN_PLAYER = 25f;
	public static final float ANGLE_BETWEEN_FLOOR = 30f;
	
	public static final float ANGLE_FLOOR_DIFFERENCE = 5f;
	
	public static final float ANGLE_BETWEEN_PLAYER = 15f;
	
	private Vector3f position;
	private float pitch;
	private float yaw;
	private float roll;
	
	private MainPlayer mainPlayer;
	
	public Camera(MainPlayer mainPlayer) {
		this.position = new Vector3f(0);
		this.mainPlayer = mainPlayer;
	}

	public void move() {
		
		Triplet<Double, Double, Boolean> mousePos = InputHandler.getMouseXZDelta();
		if(mousePos != null) {
			
			// X = yaw
			// Y = pitch
			double xPos = mousePos.getT1();
			double yPos = mousePos.getT2();
			
			float calcYaw = (float) (xPos * MOUSE_SENS);
			float offsetCalcPith = (float) (yPos * MOUSE_SENS);
			
			this.mainPlayer.rotate(0, -(calcYaw % 360), 0);
			
			float hor = (float) (DISTANCE_BETWEEN_PLAYER * Math.cos(Math.toRadians(pitch)));
			float ver = (float) (DISTANCE_BETWEEN_PLAYER * Math.sin(Math.toRadians(pitch)));
			
			calcCamPos(hor, ver, offsetCalcPith);
		}
	}
	
	public void calcCamPos(float hor, float ver, float offsetCalcPith) {
		float rotY = this.mainPlayer.rotY+ANGLE_BETWEEN_PLAYER;
		float offSetX = (float) (hor * Math.sin(Math.toRadians(rotY)));
		float offSetZ = (float) (hor * Math.cos(Math.toRadians(rotY)));
		
		this.position.x = this.mainPlayer.getPosition().x - offSetX;
		this.position.z = this.mainPlayer.getPosition().z - offSetZ;
		this.position.y = this.mainPlayer.getPosition().y + ver + 15;
		
		this.pitch -= offsetCalcPith;
		if(this.pitch < ANGLE_BETWEEN_FLOOR-ANGLE_FLOOR_DIFFERENCE) {
			this.pitch = ANGLE_BETWEEN_FLOOR-ANGLE_FLOOR_DIFFERENCE;
		}else if(this.pitch > ANGLE_BETWEEN_FLOOR+ANGLE_FLOOR_DIFFERENCE) {
			this.pitch = ANGLE_BETWEEN_FLOOR+ANGLE_FLOOR_DIFFERENCE;
			
		}
		this.yaw = 180 - this.mainPlayer.rotY;
		
	}
	
	public Vector3f getPosition() {
		return position;
	}

	public float getPitch() {
		return pitch;
	}

	public float getYaw() {
		return yaw;
	}

	public float getRoll() {
		return roll;
	}

	
	
	
	
}
