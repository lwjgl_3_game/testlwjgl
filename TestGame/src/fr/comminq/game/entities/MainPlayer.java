package fr.comminq.game.entities;

import org.joml.Vector3f;

import fr.comminq.game.models.TexturedModel;
import fr.comminq.game.utils.InputHandler;

public class MainPlayer extends Entity{

	public static final float SPEED = 0.5f;
	public static final float MOUSE_SENS = 0.15f;
	
	public MainPlayer(TexturedModel model, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
		super(model, position, rotX, rotY, rotZ, scale);
	}
	
	public void move(){
		if(InputHandler.isPaused())return;
		if(InputHandler.keyDown(org.lwjgl.glfw.GLFW.GLFW_KEY_W)) {
			float newX = (float) (SPEED * Math.sin(Math.toRadians(rotY)));
			float newZ = (float) (SPEED * Math.cos(Math.toRadians(rotY)));
			translate(newX, 0, newZ);
		}
		if(InputHandler.keyDown(org.lwjgl.glfw.GLFW.GLFW_KEY_S)) {
			float newX = (float) (SPEED * Math.sin(Math.toRadians(rotY)));
			float newZ = (float) (SPEED * Math.cos(Math.toRadians(rotY)));
			translate(-newX, 0, -newZ);
		}
		
		
		if(InputHandler.keyDown(org.lwjgl.glfw.GLFW.GLFW_KEY_D)) {
			float rotY = (this.rotY - 90) % 360;
			float newX = (float) (SPEED * Math.sin(Math.toRadians(rotY)));
			float newZ = (float) (SPEED * Math.cos(Math.toRadians(rotY)));
			translate(newX, 0, newZ);
		}
		
		if(InputHandler.keyDown(org.lwjgl.glfw.GLFW.GLFW_KEY_A)) {
			float rotY = (this.rotY + 90) % 360;
			float newX = (float) (SPEED * Math.sin(Math.toRadians(rotY)));
			float newZ = (float) (SPEED * Math.cos(Math.toRadians(rotY)));
			translate(newX, 0, newZ);
		}
		
		if(InputHandler.keyDown(org.lwjgl.glfw.GLFW.GLFW_KEY_SPACE)) {
			getPosition().y+=SPEED;
		}
		if(InputHandler.keyDown(org.lwjgl.glfw.GLFW.GLFW_KEY_LEFT_SHIFT)) {
			getPosition().y-=SPEED;
		}
	}

}
